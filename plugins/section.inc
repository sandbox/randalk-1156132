<?php

/**
 * @file
 * Wysiwyg API integration for columns filter sections module.
 */

/**
 * Implementation of hook_wysiwyg_plugin().
 */
function columns_filter_sections_section_plugin() {
  $plugins['section'] = array(
    'title' => t('Column section'),
    'vendor url' => 'http://drupal.org/project/columns_filter',
    'icon file' => 'section.gif',
    'icon title' => t('Insert a column section'),
    'settings' => array(),
  );
  return $plugins;
}

